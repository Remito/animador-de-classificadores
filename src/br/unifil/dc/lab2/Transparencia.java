package br.unifil.dc.lab2;

import javax.swing.JPanel;
import java.awt.Graphics2D;

/**
 * Write a description of class FramedArray here.
 * 
 * @author Natália K.K. e Rebeca E.I.
 * @version (não alterado)
 */
public interface Transparencia
{
    void pintar(Graphics2D g2d, JPanel contexto);
}
