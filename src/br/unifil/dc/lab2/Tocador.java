package br.unifil.dc.lab2;

import java.awt.*;
import javax.swing.BorderFactory;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.util.ListIterator;

import javax.swing.JPanel;
import java.awt.Dimension;

/**
 * Write a description of class Tocador here.
 * 
 * @author Natália Kaneshima e Rebeca Ito 
 * @version 21/04/2020
 * 
 * Finalizados:
 * 1,2,3,4,5,6,7,8,9,10(?),11(?),12(?),13,14
 * 
 * em andamento:
 * 
 */
public class Tocador extends JPanel {

    public Tocador(ListIterator<Transparencia> quadrosFilme) {
        setBackground(Color.WHITE);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        carregarFilme(quadrosFilme);
    }
    public Tocador() {
        this(null);
    }
    
    public void carregarFilme(ListIterator<Transparencia> quadrosFilme) {
        this.quadrosFilme = quadrosFilme;
        this.quadroAtual = null;
        numQuadro = 0;
    }
    
    public void avancarFilme() {
        if (quadrosFilme.hasNext()) {
            quadroAtual = quadrosFilme.next();
            numQuadro++;
        }
    }
    
    public void voltarFilme() {
        if (quadrosFilme.hasPrevious()) {
            quadroAtual = quadrosFilme.previous();
            numQuadro--;
        }
    }
    
    public void rebobinarFilme() {
        while (quadrosFilme.hasPrevious()) {
            quadroAtual = quadrosFilme.previous();
            numQuadro--;
        }
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D pincel = (Graphics2D) g;

        if (quadroAtual != null) {
            quadroAtual.pintar(pincel, this);

        }else {
            super.paintComponent(g);
            g.setFont(new Font("Arial", Font.BOLD, 30));
            g.drawString("O FILME AINDA NÃO COMEÇOU!",60,50);
        }
        g.setFont(new Font("Arial", Font.BOLD, 15));
        g.drawString("Quadro " + numQuadro,720,570);
        // ESCREVER NO CANTO INFERIOR DIREITO DA TELA "Quadro 'numQuadro'"
    }

    private int numQuadro = 0;
    private Transparencia quadroAtual = null;
    private ListIterator<Transparencia> quadrosFilme = null;
}