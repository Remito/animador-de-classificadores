package br.unifil.dc.lab2;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import java.awt.*;
import java.util.Collections;
import java.util.Optional;

import javax.swing.JButton;

public class tuso {
    //lista estática
    public static void listaEstatica(Graphics2D pincel){
        
        final  int[] posicao = {100,50};//x,y || Posição da borda
        final  int[] dimensao = {115,300};//w,h || Dimenção da borda

        final  int[] posicaoBarra = {100,50};//x,y || Posição da Barra
        final  int[] dimensaoBarra = {72,300};//w,h || Dimenção da Barra
        
        final int[] espacoBorda = {20,20};
        
        int  x = 0, y = 1, w = 0, h = 1;
        List<Integer> list = new ArrayList<Integer>(ListaGravada.lista);
        int contador = 0;                
        
        //Barra
        int maior = 0;
        for(int j = 0; j < list.size(); j++){
            if(maior < list.get(j)){
                maior = list.get(j);
            }    
        }

        for(int k = 0; k < list.size(); k++){
            int menor = maior;
            menor = list.get(k);
            
                if(list.get(k) == maior){
                    //barra maior
                    pincel.setColor(ListaGravada.getRandomColor());
                    //pincel.gerarCore(r,g,b);
                    pincel.fillRect(posicaoBarra[x] + espacoBorda[x] + (120 * k), posicaoBarra[y], dimensaoBarra[w], dimensaoBarra[h]);
                    pincel.setColor(Color.black);
                    pincel.setStroke(new BasicStroke(5));
                    pincel.drawRect(posicaoBarra[x] + espacoBorda[x] + (120 * k), posicaoBarra[y], dimensaoBarra[w], dimensaoBarra[h]);    
                }else if(menor == 1){
                    //barra menores
                    pincel.setColor(ListaGravada.getRandomColor());
                    pincel.fillRect(posicaoBarra[x] + espacoBorda[x] + (120 * k), (int)(posicaoBarra[y]+dimensaoBarra[h]-(dimensaoBarra[h]/maior)), dimensaoBarra[w], dimensaoBarra[h]/maior);
                    pincel.setColor(Color.black);
                    pincel.setStroke(new BasicStroke(5));
                    pincel.drawRect(posicaoBarra[x] + espacoBorda[x] + (120 * k),(int)(posicaoBarra[y]+dimensaoBarra[h]-(dimensaoBarra[h]/maior)), dimensaoBarra[w], dimensaoBarra[h]/maior);
                }else{
                    float div = (float)maior/menor;
                    pincel.setColor(ListaGravada.getRandomColor());
                    pincel.fillRect(posicaoBarra[x] + espacoBorda[x] + (120 * k), (int)(int)(posicaoBarra[y]+dimensaoBarra[h]-(dimensaoBarra[h]/div)), dimensaoBarra[w], (int)(dimensaoBarra[h]/div));
                    pincel.setColor(Color.black);
                    pincel.setStroke(new BasicStroke(5));
                    pincel.drawRect(posicaoBarra[x] + espacoBorda[x] + (120 * k), (int)(posicaoBarra[y]+dimensaoBarra[h]-(dimensaoBarra[h]/div)), dimensaoBarra[w], (int)(dimensaoBarra[h]/div));
                }
                //(int)(posicaoBarra[y]+dimensaoBarra[h]-(dimensaoBarra[h]/div))
                //Números
                String num = list.get(k) + "";
                if(k == 0 || k == list.size()-1){
                    pincel.setColor(Color.green);
                    pincel.setFont(new Font("Arial", Font.BOLD, 15));
                    pincel.drawString(num,posicaoBarra[x] + espacoBorda[x] + (120 * k) + 30,posicaoBarra[y] + 320);
                }else{
                    pincel.setColor(Color.blue);
                    pincel.setFont(new Font("Arial", Font.BOLD, 15));
                    pincel.drawString(num,posicaoBarra[x] + espacoBorda[x] + (120 * k) + 30,posicaoBarra[y] + 320);
                }
                
            contador = k;
        }
            //caixa
            pincel.setColor(Color.black);
            pincel.setStroke(new BasicStroke(5));
            pincel.drawRect(posicao[x], posicao[y], dimensao[w] + (120 * contador), dimensao[h]);
        
            //contador
            pincel.drawString(tuso.prettyPrintMedicoes(), 50, 440);
        }
    /*/pesquisa sequêncial
    public static void buscaSequencial(List<Integer> lista, Integer chave){
        for(int i = 0; i < lista.size(); i++){
            if( lista.get(i) == chave){
                System.out.println("A chave " + chave + " foi encontrada no índice " + i);
                break;
            }else{
                System.out.println("Procurando chave");
            }
        }
    }*/
    
    //pesquisa binária
    public static Optional<Integer> pesquisarBinario(List<Integer> lista, Integer chave) {
        System.out.println("A lista tem que estar ordenada para realizar a pesquisa binária. A lista será ordenada caso ela não eestiver.");
        bubblesort(lista);
        int acumulaMeio = 0;
        do {
            int meio = lista.size() / 2;

            if (lista.get(meio) == chave){
                System.out.println("A chave " + chave + " foi encontrada com sucesso");
                return Optional.of(meio + acumulaMeio);
            }else if (lista.get(meio) > chave){
                lista = lista.subList(0, meio);
            }else if (lista.get(meio) < chave) {
                acumulaMeio += meio + 1;
                lista = lista.subList(meio + 1, lista.size());}
        } while(lista.size() > 0);

        return Optional.empty();
    }

    //mergesort
    public static void mergesort(List<Integer> lista) {
        // Casos base
        if (lista.size() <= 1) return;
        // Casos de subdivisão recursiva
        final int idxMeioLista = lista.size() / 2;
        List<Integer> esquerda = new ArrayList<Integer>(lista.subList(0, idxMeioLista));
        List<Integer> direita = new ArrayList<Integer>(lista.subList(idxMeioLista, lista.size()));

        mergesort(esquerda);

        mergesort(direita);

        merge(lista, esquerda, direita);
    }
    private static void merge(List<Integer> lista, List<Integer> esquerda, List<Integer> direita) {
        int idxE = 0, idxD = 0, idxL = 0;
        while (idxE < esquerda.size() && idxD < direita.size()) {
            System.out.println(lista);
            operacaoRwMemoria++;
            if (esquerda.get(idxE) < direita.get(idxD)) {
                lista.set(idxL, esquerda.get(idxE));
                comparacoes++;
                idxE++;
            } else {
                lista.set(idxL, direita.get(idxD));
                comparacoes++;
                idxD++;
            }
            idxL++;
        }
        int idxF;
        List<Integer> faltantes;
        if (idxE < esquerda.size()) {
            comparacoes++;
            faltantes = esquerda;
            idxF = idxE;
        } else {
            comparacoes++;
            faltantes = direita;
            idxF = idxD;
        }

        while (idxF < faltantes.size()) {
            lista.set(idxL, faltantes.get(idxF));
            idxL++; idxF++;
        }
    }
    
    //bubblesort    
    public static void bubblesort(List<Integer> lista) {
        Gravador anim = new Gravador();
        resetMedidores();
        boolean houvePermuta;
        do {
            houvePermuta = false;
            // Sobe a bolha
            
            for (int i = 1; i < lista.size(); i++) {
                anim.gravarComparacaoSimples(lista, i, i+1);
                comparacoes++;
                if (lista.get(i-1) > lista.get(i)) {
                    System.out.println(lista);
                    permutar(lista, i - 1, i);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);
    }

    public static String prettyPrintMedicoes() {
        int total = 0;
        total = comparacoes + operacaoRwMemoria;
        return "Houve " + comparacoes + " comparacoes, e " + operacaoRwMemoria + " operacoes RW em memoria. Com o total de " + total;
    }
    //selection
    public static void selectionsort(List<Integer> lista) {
        resetMedidores();

        for (int i = 0; i < lista.size(); i++) {
            int menorIdx = encontrarIndiceMenorElem(lista, i);
            System.out.println(lista);
            permutar(lista, menorIdx, i);
        }
    }
    //insertion
    public static void insertionsort(List<Integer> lista) {
        resetMedidores();

        for (int i = 1; i < lista.size(); i++) {
            Integer elem = lista.get(i);

            int j = i; comparacoes++;
                while (j > 0 && lista.get(j-1) > elem) {
                    System.out.println(lista);
                operacaoRwMemoria++;
                lista.set(j, lista.get(j-1)); // Deslocamento

                j--; comparacoes++;
            }

            lista.set(j, elem);
        }
    }
    //quick
    public static void quicksort(List<Integer> lista) {
        quick(lista, 0, lista.size() - 1);
    }

    public static void quick(List<Integer> lista, int inicio, int fim) {
        if (inicio < fim) {
            int pivo = inicio;
            int esquerda = inicio + 1;
            int direita = fim;  
            int pivoValor = lista.get(pivo);
            while (esquerda <= direita) {
                operacaoRwMemoria++;
                System.out.println(lista);
                while (esquerda <= fim && pivoValor >= lista.get(esquerda)) {
                    comparacoes++;
                    esquerda++;
                }
                while (direita > inicio && pivoValor < lista.get(direita)) {
                    comparacoes++;
                    direita--;
                }
                if (esquerda < direita) {
                    Collections.swap(lista, esquerda, direita);
                }
            }
            Collections.swap(lista, pivo, esquerda - 1);
            quick(lista, inicio, direita - 1); // <-- pivot was wrong!
            quick(lista, direita + 1, fim);   // <-- pivot was wrong!
        }
    }
    

    private static int encontrarIndiceMenorElem(List<Integer> lista, int idxInicio) {
        int menor = idxInicio;
        for (int i = idxInicio+1; i < lista.size(); i++) {
            comparacoes++;
            if (lista.get(menor) > lista.get(i))
                menor = i;
        }
        return menor;
    }
    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a); // permutador = lista[a]
        lista.set(a, lista.get(b)); // lista[a] = lista[b]
        lista.set(b, permutador); // lista[b] = permutador

        operacaoRwMemoria+=2;
    }
    private static void resetMedidores() {
        operacaoRwMemoria = 0;
        comparacoes = 0;
    }

/*    
    public void botaoProximo(){
        //List<Integer> list = new ArrayList<Integer>(ListaGravada.lista);
        switch(ListaGravada.lista){
            case onBtnProxPressionado: 
                AnimadorAlgoritmos
            break;
        }
    }
*/
    private static int operacaoRwMemoria = 0;
    private static int comparacoes = 0;

}