package br.unifil.dc.lab2;

import java.util.List;
import java.util.ListIterator;

import javax.swing.*;

import java.awt.event.*;
import java.awt.*;

/**
 * Write a description of class AlgoritmosAnimados here.
 * 
 * @author Ricardo Inacio
 * @version 20200408
 */
public class AlgoritmosAnimados
{
    public static Gravador listaEstatica(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Valores da lista imutável");
        return anim;
    }
/**
 * O método pesquisaSequencial recebe como parâmetro uma List<Integer> e um int, onde a ele vai 
 * procurar o índice escolhido (chave)
 * 
 * O while ele soma 1 no valor i, e grava o indice destacado e o valor do i e printa  "Pesquisa 
 * sequencial", enquanto i for menor que a lista valores e o valores.get(i) for diferente da 
 * chave.
 * 
 * O if procura a cave na lista "valores" enquanto i for menor que a lista, se 
 * a chave for encontrada ele grava o indice destacado e escreve que "Chave encontrada", caso 
 * contrario ele grava a lista, printa os valores e escreve que "Chave não encontrada"; 
 * 
 * @param Gravador anim chama a classe Gravador, executando o método em que se pede 
 * dentro dele, retornando a resposta para esta classe
 * 
 * @param List<Integer> valores é uma lista de números onde vai ser procurada a chave
 * 
 * @param int chave é o índice a ser procurado
 * 
 * @return anim retorna se a chave foi encontrada ou não
 */

    public static Gravador pesquisaSequencial(List<Integer> valores, int chave) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Inicio de pesquisa sequencial");
        
        int i = 0;
        anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");
        
        while (i < valores.size() && valores.get(i) != chave) {
            i++;
            anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");
        }
        
        if (i < valores.size()) {
            anim.gravarIndiceDestacado(valores, i, "Chave encontrada");
        } else {
            anim.gravarLista(valores, "Chave não encontrada");
        }
        return anim;
        
    }

    public static Gravador pesquisaBinaria(List<Integer> valores, int chave) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Inicio de pesquisa binária");
        
        int i = 0;
        anim.gravarIndiceDestacado(valores, i, "Pesquisa binária");
        
        while (i < valores.size() && valores.get(i) != chave) {
            i++;
            anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");
        }
        
        if (i < valores.size()) {
            anim.gravarIndiceDestacado(valores, i, "Chave encontrada");
        } else {
            anim.gravarLista(valores, "Chave não encontrada");
        }
        
        return anim;
    }
    
    
    public static Gravador classificarPorBolha(List<Integer> valores) {
        Gravador anim = new Gravador();
        
        anim.gravarLista(valores, "Disposição inicial");
        AlgoritmosAnimados.listaEstatica(valores);
        tuso.bubblesort(valores);
        anim.gravarLista(valores, "Disposição final");

        System.out.println(tuso.prettyPrintMedicoes());
        return anim;
        //throw new RuntimeException("Funcionalidade ainda não implementada pelo aluno");
    }

    public static Gravador classificarPorSelecao(List<Integer> valores) {
        Gravador anim = new Gravador();
        
        anim.gravarLista(valores, "Disposição inicial");
        System.out.println("Lista não deve estar ordenada: " + valores);
        tuso.selectionsort(valores);
        anim.gravarLista(valores, "Disposição final");
        System.out.println("Lista deve estar ordenada: " + valores);
        System.out.println(tuso.prettyPrintMedicoes());
        return anim;
        //throw new RuntimeException("Funcionalidade ainda não implementada pelo aluno");
    }

    public static Gravador classificarPorInsercao(List<Integer> valores) {
        Gravador anim = new Gravador();
        
        anim.gravarLista(valores, "Disposição inicial");
        System.out.println("Lista não deve estar ordenada: " + valores);
        tuso.insertionsort(valores);
        
        anim.gravarLista(valores, "Disposição final");
        System.out.println("Lista deve estar ordenada: " + valores);
        System.out.println(tuso.prettyPrintMedicoes());
        return anim;
        //throw new RuntimeException("Funcionalidade ainda não implementada pelo aluno");
    }

    public static Gravador classificarPorJuncao(List<Integer> valores) {
        Gravador anim = new Gravador();
        
        anim.gravarLista(valores, "Disposição inicial");
        System.out.println("Lista não deve estar ordenada: " + valores);
        tuso.mergesort(valores);
        
        anim.gravarLista(valores, "Disposição final");
        System.out.println("Lista deve estar ordenada: " + valores);
        System.out.println(tuso.prettyPrintMedicoes());
        return anim;
        //throw new RuntimeException("Funcionalidade ainda não implementada pelo aluno");
    }

    public static Gravador classificarPorQuick(List<Integer> valores) {
        Gravador anim = new Gravador();
        
        anim.gravarLista(valores, "Disposição inicial");
        System.out.println("Lista não deve estar ordenada: " + valores);
        tuso.quicksort(valores);
        
        anim.gravarLista(valores, "Disposição final");
        System.out.println("Lista deve estar ordenada: " + valores);
        System.out.println(tuso.prettyPrintMedicoes());
        return anim;
        //throw new RuntimeException("Funcionalidade ainda não implementada pelo aluno");
    }
}